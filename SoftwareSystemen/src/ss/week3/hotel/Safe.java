package ss.week3.hotel;

public class Safe {
	
	private Password password;
	private boolean isActive;
	private boolean isOpen;
	
	//@requires password != null;
	public Safe(Password password) {
		this.isActive = false;
		this.isOpen = false;
		this.password = password;
	}
	
	//@requires password.length() >= 6;
	//@requires !password.contains(" ");
	//@requires (password != null);
	public boolean activate(String password){
		assert password != null;
		boolean result;
		result = this.password.testWord(password);
		this.isActive = result;
		return result;
	}
		
	//@ensures isActive() == false;
	//@ensures isOpen() == true;
	public void deactivate() {
		isActive = false;
		isOpen = false;
	}
	
	//@requires password.length() >= 6;
	//@requires !password.contains(" ");
	//@requires (password != null);
	public boolean open(String password) {
		boolean result;
		if (!this.isActive) {
			result = false;
		} else {
			result = this.password.testWord(password);
		}
		this.isOpen = result;
		return result;
	}
	
	public void close() {
		isOpen = false;
	}
	
	/*@ pure */ public boolean isActive() {
		return isActive;
	}
	
	/*@ pure */ public boolean isOpen() {
		return isOpen;
	}
	
	/*@ pure */ public Password getPassword() {
		return this.password;
	}
}
