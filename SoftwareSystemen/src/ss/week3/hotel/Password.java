package ss.week3.hotel;
import ss.week3.pw.*;

public class Password {
	
	// ------------------ Instance variables -----------------
	
	private String password;
	private Checker checker;
	private String factoryPassword;
	
	// ------------------ Constructor ------------------------
	
	public Password(Checker checker) {
		this.checker = checker;
		this.factoryPassword = checker.generatePassword();
	}
	
	public Password() {
		this(new BasicChecker());
	}
	
	// ------------------ Queries --------------------------
	/**
	 * Returns this checker.
	 * @return Checker of this Password
	 */
	public Checker getChecker() {
		return this.checker;
	}
	
	/**
	 * Returns the factory password.
	 * @return factorypasssword of this password
	 */
	public String getFactoryPassword() {
		return this.factoryPassword;
	}
	// ------------------ Commands --------------------------
	/**
	 * Checks if a password is acceptable.
	 * @param suggestion String has to have more than 6 tokens and can not contain a space.
	 * @return true if password has more than 6 tokens and does not contain a a space; false if not.
	 */
	public boolean acceptable(String suggestion) {
		return (suggestion.length() > 6) && !suggestion.contains(" ");
	}
	
	/**
	 * Tests if password matches the password of the class.
	 * @param test String to test with this password.
	 * @return
	 */
	public boolean testWord(String test) {
		return password.equals(test);
	}
	
	/**
	 * Sets a new password if the oldpassword matches.
	 * @param oldpass has to be equal to the old password.
	 * @param newpass has to obey the rules of a acceptable password.
	 * @return true if oldpassmatches and the new password is acceptable; false if not.
	 */
	public boolean setWord(String oldpass, String newpass) {
		boolean result;
		System.out.println(oldpass + " " + newpass);
		System.out.println(this.testWord(oldpass) + " " + 
				this.acceptable(newpass));
		System.out.println(password);
		if (this.testWord(oldpass) && this.acceptable(newpass)) {
			this.password = newpass;
			result = true;
			System.out.println("the password is set");
		} else {
			result = false;
			System.out.println("the password is not set");
		}
		return result;
	}
	
	public static void main(String[] args) {	
	}

}
