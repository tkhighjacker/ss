package ss.week3.hotel;

import ss.week3.hotel.Bill.Item;

public class PricedRoom extends Room implements Item {
	
	private double price;

	public PricedRoom(int no, double roomPrice, double safePrice) {
		super(no, new PricedSafe(safePrice));
		this.price = roomPrice;
	}
	
	public PricedRoom(int no) {
		this(no, 50.00, 10.00);
	}

	@Override
	public double getAmount() {
		return price;
	}
	
	@Override
	public String toString() {
		return super.toString() + ": " + this.getAmount();
	}

}
