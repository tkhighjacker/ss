package ss.week3.hotel;

public class Format {

	public Format() {
	}
	
	public static void printLine(String text, double amount) {
		String left = text;
		double right = amount;
		String value = String.format("%-10s %10.2f", left, right);
	    System.out.println(value);
	}
	
	public static void main(String[] args) {
		Format.printLine("text1", 1.00);
		Format.printLine("other text", -12.12);
		Format.printLine("something", 0.20);
	}

}
