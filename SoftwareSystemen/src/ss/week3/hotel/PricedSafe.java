package ss.week3.hotel;

import ss.week3.hotel.Bill.Item;

public class PricedSafe extends Safe implements Item {
	
	private double price;

	public PricedSafe(Password password, double price) {
		super(password);
		this.price = price;
	}
	
	public PricedSafe(Password password) {
		this(password, 10.00);
	}
	
	public PricedSafe(double price) {
		this(new Password(), price);
	}
	
	@Override
	public double getAmount() {
		return price;
	}
	
	@Override
	public String toString() {
		return "Safe: " + this.price;
	}

}
