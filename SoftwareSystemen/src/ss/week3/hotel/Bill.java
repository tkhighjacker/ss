package ss.week3.hotel;
import java.io.PrintStream;

public class Bill {
	
	
	private PrintStream output; 
	private double sum;
	/**
	 * 
	 * @param theOutStream
	 */

	public Bill(PrintStream theOutStream) {
		this.output = theOutStream;
	}
	
	public static interface Item {
		public double getAmount();
		public String toString();
	}
	
	public void newItem(Item item) {
		printLine(item.toString(), item.getAmount());
		this.sum = item.getAmount() + sum;
	}
	
	public void finish() {
		printLine("total: ", this.getSum());
	}
	
	public double getSum() {
		return sum;
	}
	
	public void printLine(String text, double amount) {
		if (this.output != null) {
			String left = text;
			double right = amount;
			String value = String.format("%-10s %10.2f", left, right);
		    output.println(value);	
		}		
	}
	
	public static void main(String[] args) {
		Bill bill = new Bill(System.out);
		bill.finish();
		
		
	}
}
