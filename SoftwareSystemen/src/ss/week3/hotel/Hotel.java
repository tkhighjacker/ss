package ss.week3.hotel;
import java.io.PrintStream;

public class Hotel {
	
	private Password hPassword = new Password();
	private PricedRoom room1;
	private PricedRoom room2;
	private String hotelName;
	
	//@requires name != null;
	public Hotel(String name) {
		this.hotelName = name;
		room1 = new PricedRoom(1);
		room2 = new PricedRoom(2);
	}
	/*@
	  requires password != null;
	  requires guestName != null;
	  ensures \forall String guestnames; \result == null ||
	  \result == getRoom(guestnames); guestnames != null;  
	 */
	public Room checkIn(String password, String guestName) {
		Room result;
		if (room1.getGuest() == null && this.hPassword.testWord(password) && 
				this.getRoom(guestName) == null) {
			Guest guest = new Guest(guestName);
			guest.checkin(room1);
			result = room1;
		} else if (room2.getGuest() == null && this.hPassword.testWord(password) && 
				this.getRoom(guestName) == null) {
			Guest guest = new Guest(guestName);
			guest.checkin(room2);
			result = room2;
		} else {
			result = null;
		}
		return result;
	}
	//@ensures this.getRoom(guest) == null;
	//@ensures this.getRoom(guest).getSafe() == null;
	public void checkOut(String guest) {
		if (this.getRoom(guest) != null) {
			this.getRoom(guest).getSafe().deactivate();
			this.getRoom(guest).getGuest().checkout();
		}
	}
	
	/*@ pure */ public Room getFreeRoom() {
		Room result;
		if (this.room1.getGuest() == null) {
			result = room1;
		} else if (this.room2.getGuest() == null) {
			result = room2;
		} else {
			result = null;
		}
		return result;
	}
	/*@ pure */ public PricedRoom getRoom(String guest) {
		PricedRoom result;
		if (room1.getGuest() != null && room1.getGuest().getName().equals(guest)) {
			result = room1;
		} else if (room2.getGuest() != null && room2.getGuest().getName().equals(guest)) {
			result = room2;
		} else {
			result = null;
		}
		return result;
	}
	
	/*@ pure */ public Bill getBill(String name, int nights, PrintStream output) {
		Bill result = new Bill(output);
		if (this.getRoom(name) != null) {
			result.newItem(this.getRoom(name));
			result.newItem(this.getRoom(name).getPricedSafe());
			result.finish();
		} else {
			result = null;
		}
		return result;
	}
	
	/*@ pure */ public Password getPassword() {
		return this.hPassword;
	}
	
	/*@ pure */ public String getHotelName() {
		return this.hotelName;
	}
	
	/*@ pure */ public String toString() {
		String result = "HotelGasten:";
		if (room1 != null) {
			result = result + (" " + room1.getGuest().getName() + 
					" room number " + room1.getNumber());
		} else if (room2 != null) {
			result = result + (" " + room2.getGuest().getName() + 
					" room number " + room2.getNumber());
		} else {
			result = result + " geen";
		}
		return result;
	}
}
