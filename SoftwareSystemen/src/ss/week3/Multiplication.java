package ss.week3;

public class Multiplication implements OperatorWithIdentity {

	public Multiplication() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int operate(int left, int right) {
		return left * right;
	}

	@Override
	public int identity() {
		return 1;
	}

}
