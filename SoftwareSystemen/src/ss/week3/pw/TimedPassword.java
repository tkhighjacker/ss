package ss.week3.pw;

public class TimedPassword extends Password {
	
	private int validTime;
	private long startTime;
	

	public TimedPassword(int time) {
		super();
		this.validTime = time;
		this.startTime = System.currentTimeMillis();
	}

	public TimedPassword() {
		this(10000);
	}
	
	// ------------------ Queries ---------------------------
	public boolean isExpired() {
		return validTime + startTime <= System.currentTimeMillis();
	}
	
	// ------------------ Commands --------------------------
	public boolean testWord(String test) {
		return super.testWord(test) && !this.isExpired();
	}
	
	public boolean setWord(String oldpass, String newpass) {
		boolean result;
		if (super.acceptable(oldpass) && super.acceptable(newpass)) {
			this.startTime = System.currentTimeMillis();
			result = true;
			System.out.println("result is true");
		} else {
			result = false;
			System.out.println("result is false");
		}
		return result;
	}
	
	public static void main(String[] args) {
		
	}

}
