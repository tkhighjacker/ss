package ss.week3.pw;

public interface Checker {
	/**
	 * Checks if password is an acceptable password.
	 * @param password that is an acceptable password
	 * @return true if the password is acceptable; false if the password is unacceptable
	 */
	public boolean acceptable(String password);
	
	/**
	 * generate a acceptable password.
	 * @return password that is acceptable
	 */
	public String generatePassword();

}
