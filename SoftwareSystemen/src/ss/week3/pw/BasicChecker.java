package ss.week3.pw;

public class BasicChecker implements Checker {
	
	public static final String INITPASS = "initial";

	public boolean acceptable(String password) {
		return (password.length() > 6) && !password.contains(" ");
	}
	
	public String generatePassword() {
		return INITPASS;
	}

}
