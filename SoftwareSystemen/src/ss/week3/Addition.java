package ss.week3;

public class Addition implements OperatorWithIdentity {

	public Addition() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int operate(int left, int right) {
		
		return left + right;
	}

	@Override
	public int identity() {
		return 0;
	}

}
