package ss.week7;

public class IntCell {
    private int contents = 0;

    synchronized public void add(int amount) {
        contents = contents + amount;
    }
    synchronized public int get() {
        return contents;
    }

    public static void main(String[] args) throws InterruptedException {
        IntCell cell = new IntCell();
        Adder a1 = new Adder(cell, 1);
        Adder a2 = new Adder(cell, 2);
        a1.start();
        a2.start();
        a1.join();
        a2.join();
        System.out.println(cell.get());
    }
}

class Adder extends Thread {
    private IntCell cell;
    private int amount;

    public Adder(IntCell cellArg, int amountArg) {
        this.cell = cellArg;
        this.amount = amountArg;
    }
    public void run() {
        cell.add(amount);
    }
}
