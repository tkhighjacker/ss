
package ss.week7.cmdline;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Server. 
 * @author  Theo Ruys
 * @version 2005.02.21
 */
public class Server {
	
    private static final String USAGE
        = "usage: " + Server.class.getName() + " <name> <port>";

    /** Starts a Server-application. */
    public static void main(String[] args) {
    	if (args.length != 2) {
    		System.out.println(USAGE);
    		System.exit(0);
    	}
    	String host = args[0];
    	int port = Integer.parseInt(args[1]);
    	ServerSocket sSock = null;
    	Socket sock = null;
    	Peer server = null;
    	try {
    		sSock = new ServerSocket(port);
    		sock = sSock.accept();
    		server = new Peer(host, sock);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.out.println(USAGE);
		}
    	Thread streamInputHandler = new Thread(server);
    	streamInputHandler.start();
    	server.handleTerminalInput();
    }

} // end of class Server
