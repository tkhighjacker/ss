package ss.week7.threads;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestSyncConsole extends Thread {
	
	private Lock lock;
	
	public TestSyncConsole(String name) {
		super(name);
		lock = new ReentrantLock();
	}
	
	public void run() {
		this.sum();
	}
	
	public void sum() {
		this.lock.lock();
		int a = SyncConsole.readInt(this.getName() + ": Get number 1? ");
		int b = SyncConsole.readInt(this.getName() + ": Get number 2? ");
		int c = a + b;
		System.out.println(this.getName() + ": " + a + " " + b + " = " + c);
		this.lock.unlock();
	}
	
	public static void main (String[] args) throws InterruptedException {
		TestConsole a = new TestConsole("Thread A");
		TestConsole b = new TestConsole("Thread B");
		a.start();
		a.join();
		b.start();
		b.join();
	}
}
