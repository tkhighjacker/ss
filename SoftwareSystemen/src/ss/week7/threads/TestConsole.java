package ss.week7.threads;

public class TestConsole extends Thread {
	
	public TestConsole(String name) {
		super(name);
	}
	
	public void run() {
		sum();
	}
	
	public void sum() {
		int a = Console.readInt(this.getName() + ": Get number 1? ");
		int b = Console.readInt(this.getName() + ": Get number 2? ");
		int c = a + b;
		System.out.println(this.getName() + ": " + a + " " + b + " = " + c);
	}
	
	public static void main (String[] args) {
		TestConsole a = new TestConsole("Thread A");
		TestConsole b = new TestConsole("Thread B");
		a.start();
		b.start();
	}
}
