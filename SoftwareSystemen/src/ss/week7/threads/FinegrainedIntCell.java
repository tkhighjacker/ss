package ss.week7.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FinegrainedIntCell implements IntCell {
	
	private Lock lock = new ReentrantLock();
	private Condition notEmpty = lock.newCondition();
	private Condition notFull = lock.newCondition();
	private int value = 0;
	private boolean isUnconsumed;

	@Override
	public void setValue(int val) {
		this.lock.lock();
		try {
			while (isUnconsumed) {
				this.notFull.await();
			}
			this.value = val;
			this.isUnconsumed = true;
			this.notEmpty.signal();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public int getValue() {
		this.lock.lock();
		try {
			while (!isUnconsumed) {
				this.notEmpty.await();
			}
			this.isUnconsumed = false;
			this.notFull.signal();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.lock.unlock();
		}
		return this.value;
	}
}
