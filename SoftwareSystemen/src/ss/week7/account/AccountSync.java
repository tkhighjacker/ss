package ss.week7.account;

public class AccountSync {

	public AccountSync() {
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		Account account = new Account();
		Thread threadA = new MyThread(-200, 10, account);
		Thread threadB = new MyThread(400, 10, account);
		threadA.start();
		threadB.start();
		System.out.println("The balance = " + account.getBalance());
	}

}
