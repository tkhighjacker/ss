package ss.week7.account;

public class Account {
	protected double balance = 0.0;
	static final int NEGATIVE = -1000;
	

	synchronized public void transaction(double amount) {
		while ((this.balance + amount) < NEGATIVE) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
		System.out.println(this.balance + " + " + amount + " = " + (balance + amount));
		balance = balance + amount;
		this.notify();
		
		
	}
	public double getBalance() {
		return balance;

	}
}
