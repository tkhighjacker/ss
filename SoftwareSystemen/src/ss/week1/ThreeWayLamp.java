package ss.week1;

public class ThreeWayLamp {
	
	public static final int OFF = 0;
	public static final int LOW = 1;
	public static final int HIGH = 2;
	private int setting;
	
	//@ private invariant setting <= 2 && setting <= 0;
	
	//@ ensures getSetting() == OFF;
	public ThreeWayLamp() {
		setting = OFF;
	}
	
	/*@ pure */ public int getSetting() {
		return setting;
	}
	
	/*@
	  ensures (\old(getSetting()) == OFF) ==> (getSetting()) == LOW &&
	  \old(getSetting()) == LOW ==> (getSetting()) == HIGH &&
	  \old(getSetting()) == HIGH ==> (getSetting()) == OFF;
	 */
	public void changeSetting() {
		setting = (setting + 1) % 3;
	}
	
	//@ ensures getSetting() == OFF;
	public void resetSetting() {
		setting = OFF;
	}
	
	public static void main(String[] args) {
		ThreeWayLamp lamp = new ThreeWayLamp(); 
		lamp.changeSetting();
		lamp.changeSetting();
		lamp.changeSetting();
		System.out.println(lamp.getSetting());
	}

}
