package ss.week1;

public class ThreeWayLampTest {
	
	private ThreeWayLamp lamp;
	
	public ThreeWayLampTest() {
		lamp = new ThreeWayLamp();
	}
	
	public void runTest() {
		testInitialState();
		testChange();
		testReset();
	}
	
	private void testInitialState() {
		System.out.println("Test Initial State");
		System.out.println("initial Light:" + lamp.getSetting());
	}
	
	private void testChange() {
		System.out.println("Test Change:");
		lamp.changeSetting();
		System.out.println("Setting after 1 change = " + lamp.getSetting());
		lamp.changeSetting();
		System.out.println("Setting after 2 change = " + lamp.getSetting());
		lamp.changeSetting();
		System.out.println("Setting after 3 change = " + lamp.getSetting());
	}
	private void testReset() {
		System.out.println("Test Reset:");
		lamp.changeSetting();
		System.out.println("Setting after 1 change = " + lamp.getSetting());
		lamp.resetSetting();
		System.out.println("Setting after reset = " + lamp.getSetting());
	}
}
