package ss.week1;

public class Password {
	
	// ------------------ Instance variables -----------------
	
	public static final String INITIAL = "initial";
	private String password;
	
	// ------------------ Constructor ------------------------
	
	public Password() {
		this.password = INITIAL;
	}
	
	// ------------------ Queries --------------------------
	
		
	
	// ------------------ Commands --------------------------
	/**
	 * Checks if a password is acceptable.
	 * @param suggestion String has to have more than 6 tokens and can not contain a space.
	 * @return true if password has more than 6 tokens and does not contain a a space; false if not.
	 */
	public boolean acceptable(String suggestion) {
		return (suggestion.length() > 6) && !suggestion.contains(" ");
	}
	
	/**
	 * Tests if password matches the password of the class.
	 * @param test String to test with this password.
	 * @return
	 */
	public boolean testWord(String test) {
		return password.equals(test);
	}
	
	/**
	 * Sets a new password if the oldpassword matches
	 * @param oldpass has to be equal to the old password.
	 * @param newpass has to obey the rules of a acceptable password.
	 * @return true if oldpassmatches and the new password is acceptable; false if not.
	 */
	public boolean setWord(String oldpass, String newpass) {
		boolean result;
		if (this.testWord(oldpass) && this.acceptable(newpass)) {
			this.password = newpass;
			result = true;
		}
		else result = false;
		return result;
	}
	
	

}
