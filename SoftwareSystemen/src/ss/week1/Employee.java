package ss.week1;

public class Employee {
	
	private int hours;
	private double rate;
	
	public double pay() {
		double payment;
		if (hours <= 40) {
			payment = (hours * rate);
		}
		else {
			payment = (40 * rate) + ((hours - 40) * 0.5 * rate);
		}
		return payment;
	}
}
