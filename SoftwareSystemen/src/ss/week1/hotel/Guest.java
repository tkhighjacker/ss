package ss.week1.hotel;
/**
 * Hotel room with number and possibly a guest.
 * @author Tibor Casteleijn
 * @version $1.0 $
 */
public class Guest {
	// ------------------ Instance variables -----------------
	
	private String name;
	private Room room;
	
	// ------------------ Constructor ------------------------

    /**
     * Creates a <code>Guest</code> with the given name.
     * @param n name of <code>Guest</code>
     */
	public Guest(String n) {
		this.name = n;
	}
	
	// ------------------ Queries --------------------------

    /**
     * @return the name of this <code>Guest</code>.
     */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the Room that is rented by this <code>Guest</code>.
	 * @return Room rented by thus <code>Guest</code>; 
	 * <code>null</code> if this <code>Guest</code> does not rent a room
	 */
	public Room getRoom() {
		return this.room;
	}
	
	// ------------------ Commands --------------------------
	
	/**
	 * Rents a <code>Room</code> to this <code>Guest</code>. 
	 * This is only possible if this <code>Guest</code> does not already have a <code>Room</code>. 
	 * And the <code>Room</code> to be assigned is not already rented. 
	 * Also adapts the <code>Guest</code>-reference of the <code>Room</code>.
	 * @param r <code>Room</code> to be rented to this <code>Guest</code>; may not be <code>null</code>
	 * @return true if checkin succeeded; false if this Guest already had a Room, 
	 * or r already had a <code>Guest</code>.
	 */
	public boolean checkin(Room r) {
		boolean result;
		if (r.getGuest() == null) {
			this.room = r;
			this.room.setGuest(this);
			result = true;
		}
		else {
			result = false;
		}
		return result;
	}
	
	/**
	 * Sets the <code>Room</code> of this <code>Guest</code> to <code>null</code>.
	 * Also resets the <code>Guest</code>-reference of the (current) <code>Room</code>.
	 * @return true if this action succeeded;
	 * false if Guest does not have a Room when this method is called
	 */
	public boolean checkout() {
		boolean result;
		if (this.room != null) {
			this.room.setGuest(null);
			this.room = null;
			result = true;
		}
		else {
			result = false;
		}
		return result;
		
	}
	
	/**
	 * <code>toString</code> in class <code>java.lang.Object</code>
	 */
	public String toString() {
		return "Guest " + this.getName();
	}
}
