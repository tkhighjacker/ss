package ss.week1;

public class DollarsAndCentsCounter {
	
	private int dollars;
	private int cents;
	
	public int dollars() {
		return this.dollars;
	}
	
	public  int cents() {
		return this.cents;
	}
	
	public void add(int dollars, int cents) {
		int centsCounter = cents + this.cents;
		while (centsCounter >= 100) {
			this.dollars = this.dollars + 1;
			centsCounter = centsCounter - 100; 
		}
		this.dollars = this.dollars + dollars;
		this.cents = centsCounter;
	}
	
	public void reset() {
		dollars = 0;
		cents = 0;
	}
}
