package ss.week1;

public class ThreeWayLamp2 {
	
	private Setting setting;
	
	public enum Setting {
		OFF, LOW, HIGH
	}
	
	public ThreeWayLamp2(Setting setting) {
		this.setting = setting;
	}
	
	/*@ pure */ public Setting getSetting() {
		return this.setting;
	}
	
	/*@
	  ensures (\old(getSetting()) == getSetting() ==> (getSetting()) == getSetting() &&
	  \old(getSetting()) == getSetting() ==> (getSetting()) == getSetting() &&
	  \old(getSetting()) == getSetting() ==> (getSetting()) == getSetting());
	 */
	public void changeSetting() {
		switch (setting) {
		case OFF:
			System.out.println("The light is now turned on and on low");
			this.setting = setting.LOW;
			break;
		case LOW:
			System.out.println("The light is now turned on and on high");
			this.setting = setting.HIGH;
			break;
		case HIGH:
			System.out.println("The light is now turned off");
			this.setting = setting.LOW;
			break;
		}
	}
	
	public void resetSetting() {
		setting = setting.OFF;
	}

}
