package ss.week2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
/**
 * 
 * @author tkcas
 * test program for rectangle
 */
public class RectangleTest {
	
	private Rectangle rectangle;
	
	@Before
	public void setup() {
		this.rectangle = new Rectangle(3, 2);
	}
	
	@Test
	public void testArea() {
		assertEquals(rectangle.area(), 6);
	}
	
	@Test
	public void testPerimeter() {
		assertEquals(rectangle.perimeter(), 10);
	}

}
