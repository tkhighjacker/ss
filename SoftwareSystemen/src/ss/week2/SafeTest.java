package ss.week2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ss.week3.hotel.Password;
import ss.week3.hotel.Safe;

public class SafeTest {
	
	private Safe safe;
	private Password password;
	
	@Before
	public void setUp() throws Exception {
		password = new Password();
		safe = new Safe(password);
	}

	@Test
	public void testActivate() {
		//activate non active safe with correct password
		assertTrue(safe.activate("initial"));
		
		//activate non active safe with incorrect password
		assertFalse(safe.activate("notInitial"));
	}
	
	@Test
	public void testDeactivate() {
		//deactivate deactive safe
		safe.deactivate();
		assertFalse(safe.isActive());
		assertFalse(safe.isOpen());
		
		//deactivate active safe
		safe.activate("initial");
		safe.deactivate();
		assertFalse(safe.isActive());
		assertFalse(safe.isOpen());
	}
	
	@Test
	public void testOpen() {
		//open non-active safe
		safe.open("initial");
		assertFalse(safe.isOpen());
		
		//open active non-open safe
		safe.activate("initial");
		safe.open("initial");
		assertTrue(safe.isOpen());
		
		//open active open safe
		safe.open("initial");
		assertTrue(safe.isOpen());
	}
	
	@Test
	public void testClose() {
		//close closed safe
		safe.close();
		assertFalse(safe.isOpen());
		
		//close active safe
		safe.activate("initial");
		safe.close();
		assertFalse(safe.isOpen());
		
		//close active and open safe
		safe.activate("initial");
		safe.open("initial");
		safe.close();
		assertFalse(safe.isOpen());
	}
	
	@Test
	public void testIsActive() {
		assertFalse(safe.isActive());
	}
	
	@Test
	public void testIsOpen() {
		assertFalse(safe.isOpen());
	}
	
	@Test
	public void testGetPassword() {
		assertEquals(safe.getPassword(), this.password);
	}

}
