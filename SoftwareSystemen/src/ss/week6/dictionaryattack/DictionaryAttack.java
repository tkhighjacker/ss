package ss.week6.dictionaryattack;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class DictionaryAttack {
	private Map<String, String> passwordMap;
	private Map<String, String> hashDictionary;

	/**
	 * Reads a password file. Each line of the password file has the form:
	 * username: encodedpassword
	 * 
	 * After calling this method, the passwordMap class variable should be
	 * filled withthe content of the file. The key for the map should be
	 * the username, and the password hash should be the content.
	 * @param filename
	 */
	public void readPasswords(String filename) {
		try {
			Map<String, String> tempPasswordMap = new HashMap<String, String>();
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String input = "";
			while ((input = reader.readLine()) != null) { 
				String[] userPass = input.split(": ");
				tempPasswordMap.put(userPass[0], userPass[1]);
			}
			this.passwordMap = tempPasswordMap;
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Given a password, return the MD5 hash of a password. The resulting
	 * hash (or sometimes called digest) should be hex-encoded in a String.
	 * @param password
	 * @return
	 */
	public String getPasswordHash(String password) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] passBytes = password.getBytes();
		md.update(passBytes, 0, passBytes.length);
		String result = new BigInteger(1,md.digest()).toString(16);
		return result;
	}
	/**
	 * Checks the password for the user the password list. If the user
	 * does not exist, returns false.
	 * @param user
	 * @param password
	 * @return whether the password for that user was correct.
	 */
	public boolean checkPassword(String user, String password) {
		return passwordMap.containsKey(user) && passwordMap.get(user).equals(this.getPasswordHash(password));
		
	}

	/**
	 * Reads a dictionary from file (one line per word) and use it to add
	 * entries to a dictionary that maps password hashes (hex-encoded) to
     * the original password.
	 * @param filename filename of the dictionary.
	 */
    	public void addToHashDictionary(String filename) {
    		Map<String, String> tempHashDictionary = new HashMap<String, String>();
    		try {
    			BufferedReader reader = new BufferedReader(new FileReader(filename));
				while (reader.ready()) {
					String input = reader.readLine();
					String[] split = input.split(": ");
					tempHashDictionary.put(split[1], split[0]);
				}
				reader.close();
				this.hashDictionary = tempHashDictionary;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		
    }
	/**
	 * Do the dictionary attack.
	 */
	public void doDictionaryAttack() {
		this.readPasswords("LeakedPasswords.txt");
		this.addToHashDictionary("Passwords.txt");
		for (String s : this.passwordMap.keySet()) {
			if (this.hashDictionary.containsKey(this.passwordMap.get(s)))
				System.out.println(s + " password: " + this.hashDictionary.get(this.passwordMap.get(s)));
		}
	}
	public static void main(String[] args) {
		DictionaryAttack da = new DictionaryAttack();
		// To implement
		da.doDictionaryAttack();
	}

}
