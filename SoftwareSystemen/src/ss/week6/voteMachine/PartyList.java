package ss.week6.voteMachine;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class PartyList extends Observable {
	
	private List<String> partyList;

	public PartyList() {
		partyList = new ArrayList<String>();
	}
	
	public void addParty(String party) {
		partyList.add(party);
		this.setChanged();
		this.notifyObservers(party);
	}
	
	public List<String> getParties() {
		return partyList;
	}
	
	public boolean hasParty(String party) {
		return partyList.contains(party);
	}
}
