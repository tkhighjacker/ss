package ss.week6.voteMachine;

import ss.week6.voteMachine.gui.VoteGUIView;

public class VoteMachine {
	
	VoteList voteList;
	PartyList partyList;
	VoteView view;

	public VoteMachine() {
		this.partyList = new PartyList();
		this.voteList = new VoteList();
		this.view = new VoteGUIView(this);
		this.voteList.addObserver(view);
		this.partyList.addObserver(view);
	}
	
		
	public void addParty(String party) {
		if (!this.partyList.hasParty(party)) {
			partyList.addParty(party);
		}
	}
	
	public void vote(String party) {
		if (partyList.hasParty(party)) {
			voteList.addVote(party);
		}
	}
	
	public void start() {
		this.view.start();
	}
	
	public PartyList getParties() {
		return this.partyList;
	}
	
	public VoteList getVotes() {
		return this.voteList;
	}
	
	public static void main (String[] args) {
		VoteMachine voteMachine = new VoteMachine();
		voteMachine.start();
	}

}
