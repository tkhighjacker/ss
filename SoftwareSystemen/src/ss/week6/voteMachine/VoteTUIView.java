package ss.week6.voteMachine;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class VoteTUIView implements Observer, VoteView {
	
	private VoteMachine voteMachine;

	public VoteTUIView(VoteMachine voteMachine) {
		this.voteMachine = voteMachine;
	}
	
	public void start() {
		String help = "Use the following commands for the vote machine \n\n"
				+ "VOTE [the name of the paty] \n"
				+ "ADD PARTY [the name of the party] \n"
				+ "EXIT to stop the votemachine \n"
				+ "HELP to get info about how this program works";
		System.out.println(help);
		Scanner scanner = new Scanner(System.in);
		boolean running = true;
		String word1 = "";
		String word2 = "";
		String[] split;
		while (running) {
			if (scanner.hasNext()) {
				String input = scanner.nextLine();
				split = input.split(" ", 3);
				if (split.length == 1) {
					word1 = split[0].toUpperCase();
					if (word1.contains("PARTIES")) {
						System.out.println(voteMachine.getParties().getParties().toString());
					} else if (word1.contains("EXIT")) {
						System.out.println("Votemachine is shutting down...");
						running = false;
					} else if (word1.contains("HELP")) {
						System.out.println(help);
					} else if (word1.contains("VOTES")){
						System.out.println(voteMachine.getVotes().getVotes().toString());
					} else {
						System.out.println(help);
					}
				}
				else if (split.length == 2) {
					word1 = split[0].toUpperCase();
					word2 = split[1].toUpperCase();
					if (word1.contains("VOTE")) {						
						voteMachine.vote(split[1]);
					}
				} else if (split.length > 2) {
					word1 = split[0].toUpperCase();
					word2 = split[1].toUpperCase();
					if (word1.equals("ADD") && word2.equals("PARTY")) {
						voteMachine.addParty(split[2]);
					} else {
						System.out.println(help);
					}
				}
			}
		}
		scanner.close();
	}
	
	public void showVotes (Map<String, Integer> map) {
		for (String party: map.keySet()) {
			System.out.println(party + " has "+ map.get(party) + " votes");
		}
	}
	
	public void showParties (List<String> list) {
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			int i = 1;
			System.out.println(i + " PARTY " + it.next());
			i++;
		}
	}
	
	public void showError (String error) {
		System.out.println(error);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1.equals("vote")) {
			System.out.println("The vote has correctly been added");
		} else {
			System.out.println("The party " + arg1 + " has correctly been added");
		}
	}
}
