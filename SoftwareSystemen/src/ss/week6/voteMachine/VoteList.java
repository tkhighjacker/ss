package ss.week6.voteMachine;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class VoteList extends Observable {
	
	private Map<String, Integer> voteList = new HashMap<String, Integer>();
	
	public VoteList() {
		voteList = new HashMap<String, Integer>();
	}
	
	public void addVote(String party) {
		int votes = 1;
		if (voteList.containsKey(party)) {
			votes = voteList.get(party) + 1;
		}
		voteList.put(party, votes);
		this.setChanged();
		this.notifyObservers("vote");
	}
	
	public Map<String, Integer> getVotes() {
		return this.voteList;
	}

}
