package ss.week6;

import java.util.Scanner;

public class Words {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Type in your sentence");
		String sentence = "";
		while (input.hasNext()) {
			sentence = input.nextLine();
			if (sentence.equals("end")) {
				break;
			}
			System.out.println(sentence);
			String[] sentArray = sentence.split(" ");
			for (int i = 0; i < sentArray.length; i++) {
				System.out.println("Word " + (i + 1) + ": " + sentArray[i]);
			}
		}
		input.close();
	}
}
