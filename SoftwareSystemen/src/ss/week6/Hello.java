package ss.week6;

import java.util.Scanner;

public class Hello {
	
	private Scanner input = new Scanner(System.in);
	private String name;

	public Hello() {
		System.out.println("Fill in a name please");
		while (name == null) {
			this.name = input.nextLine();	
		}
		System.out.println("Hello " + name);
		input.close();
	}
	
	public static void main(String[] args) {
		Hello hello = new Hello();
	}

}
