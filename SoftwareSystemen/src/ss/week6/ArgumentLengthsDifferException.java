package ss.week6;

public class ArgumentLengthsDifferException extends WrongArgumentException {

	public ArgumentLengthsDifferException(int i, int j) {
		super("error: length of command line arguments "
                + "differs (" + i + ", " + j + ")");
	}
}
