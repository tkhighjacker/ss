package ss.week5;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapUtil {
    public static <K, V> boolean isOneOnOne(Map<K, V> map) {
    	boolean result = true;
    	for (K key: map.keySet()) {
    		V checkValue = map.get(key);
    		int counter = 0;
    		for (V value: map.values()) {
    			if (checkValue.equals(value)) {
    				counter++;
    			}
    		}
    		if (counter > 1) {
    			result = false;
    		}
    	}
        return result;
    }
    public static <K, V> boolean isSurjectiveOnRange(Map<K, V> map, Set<V> range) {
        boolean result = true;
        for (V value: range) {
        	if (!map.containsValue(value)) {
        		result = false;
        	}
        }
        return result;
    }
    public static <K, V> Map<V, Set<K>> inverse(Map<K, V> map) {
        Map<V, Set<K>> result = new HashMap<V, Set<K>>();
        for (K k: map.keySet()) {
        	V value = map.get(k);
        	if (!result.containsKey(value)) {
        		result.put(value, new HashSet<K>());
        		}
        	result.get(value).add(k);
        	}
        
        return result;
        } 
       
	public static <K, V> Map<V, K> inverseBijection(Map<K, V> map) {
		Map<V, K> result = new HashMap<V, K>();
        if (MapUtil.isOneOnOne(map)) {
        	for (K k: map.keySet()) {
        		V value = map.get(k);
        		result.put(value, k);
        		}
        	}
        return result;
        }
        		
	public static <K, V, W> boolean compatible(Map<K, V> f, Map<V, W> g) {
        boolean result = true;
        for (V value: f.values()) {
        	if (!g.containsKey(value)) {
        		result = false;
        	}
        }
        return result;
	}
	public static <K, V, W> Map<K, W> compose(Map<K, V> f, Map<V, W> g) {
		Map<K, W> result = new HashMap<K, W>();
        if (MapUtil.compatible(f, g)) {
        	for (K key: f.keySet()) {
        		V gkey = f.get(key);
        		W value = g.get(gkey);
        		result.put(key, value);
        	}
        }
        return result;
	}
}
