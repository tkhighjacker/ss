package ss.week5;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

/**
 * A simple class that experiments with the Hex encoding
 * of the Apache Commons Codec library.
 *
 */
public class EncodingTest {
    public static void main(String[] args) throws DecoderException {
        String input = "Hello World";
        String bigInput = "Hello Big World";
        
        //5.11
        System.out.println("5.11");
        System.out.println(Hex.encodeHexString(bigInput.getBytes()));
        //hexadicimal becomes larger        
        
        //5.12
        System.out.println("5.12");
        char[] encodedBytes = Hex.encodeHex(input.getBytes());
        System.out.println(encodedBytes);
        byte[] byteArray = Hex.decodeHex(encodedBytes);
        String result = new String(byteArray);
        System.out.println(result);
        
        //5.13
        System.out.println("5.13");
        System.out.println(Base64.encodeBase64String(input.getBytes()));  
        System.out.println(Base64.encodeBase64String("010203040506".getBytes()));
        System.out.println(new String(Base64.decodeBase64("U29mdHdhcmUgU3lzdGVtcw==")));
        String resultA = "a";
        for (int i = 0; i < 10; i++) {
        	resultA = resultA + "a";
        	System.out.println(Base64.encodeBase64String((resultA).getBytes()));
        }
        //it grows
    }
}
