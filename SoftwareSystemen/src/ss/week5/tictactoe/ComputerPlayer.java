package ss.week5.tictactoe;

public class ComputerPlayer extends Player {
	
	Strategy strategy;
	
	public ComputerPlayer(Mark mark, Strategy strategy) {
		super(strategy.getName() + "-computer-" + mark, mark);
		this.strategy = strategy;
	}
	
	public ComputerPlayer(Mark mark) {
		this(mark, new NaiveStrategy());
	}

	@Override
	public int determineMove(Board board) {
		return this.strategy.determineMove(board, this.getMark());
	}
	
	public Strategy getStrategy() {
		return this.strategy;
	}
	
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

}
