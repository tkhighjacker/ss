package ss.week5.tictactoe;

/**
 * Executable class for the game Tic Tac Toe. The game can be played against the
 * computer. Lab assignment Module 2
 * 
 * @author Theo Ruys
 * @version $Revision: 1.4 $
 */
public class TicTacToe {
	
	
    public static void main(String[] args) {
    	if (args.length == 2) {
    		Game game = new Game(TicTacToe.createPlayer(args[0], Mark.XX), 
    				TicTacToe.createPlayer(args[1], Mark.OO));
    		game.start();
    	}
    }
    	
    public static Player createPlayer(String args, Mark mark) {
    	Player result;
    	if (args.equals("-N")) {
    		result = new ComputerPlayer(mark, new NaiveStrategy());
    	} else if(args.equals("-S")) {
    		result = new ComputerPlayer(mark, new SmartStrategy());
    	} else {
    		result = new HumanPlayer(args, mark);
    	}
    	return result;
    }
}
