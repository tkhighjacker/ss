package ss.week5.tictactoe;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SmartStrategy implements Strategy {
	
	String name;

	public SmartStrategy() {
		this.name = "Smart";
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int determineMove(Board b, Mark m) {
		int result = -1;
		Set<Integer> emptyFields = this.getEmptyFields(b);
		for (int i: emptyFields) {
			Board copy = b.deepCopy();
			copy.setField(i, m);
			if (copy.isEmptyField(4)) {
				return 4;
			} 
			else if (copy.isWinner(m)) {
				return i;					
			} else if (copy.isWinner(m.other())) {
				System.out.println("is other winner field");
				System.out.println(result);
				return i;
			} else {
				return i;
			}
		}
		return result;
	}
	
	public Set<Integer> getEmptyFields(Board b) {
		Set<Integer> emptyFields = new HashSet<>();
		for (int i = 0; i < Board.DIM * Board.DIM; i++) {
			if (b.isEmptyField(i)) {
				emptyFields.add(i);
			}
		}
		return emptyFields;
	}

}
