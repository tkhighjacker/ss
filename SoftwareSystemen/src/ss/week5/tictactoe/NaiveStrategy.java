package ss.week5.tictactoe;

import java.util.*;

public class NaiveStrategy implements Strategy {
	
	private String name;

	public NaiveStrategy() {
		this.name = "Naive";
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int determineMove(Board b, Mark m) {
		int result = -1;
		Set<Integer> emptyFields = new HashSet<>();
		for (int i = 0; i < Board.DIM * Board.DIM; i++) {
			if (b.isEmptyField(i)) {
				emptyFields.add(i);
			}
		}
		
		Iterator<Integer> it = emptyFields.iterator();
		int field = (int) (Math.random() * emptyFields.size());
		for (int i = 0; i < field && it.hasNext() && emptyFields.size() != 1; i++) {
			result = it.next();
		}
		if (emptyFields.size() == 1) {
			result = it.next();
		}
		return result;
	}

}
