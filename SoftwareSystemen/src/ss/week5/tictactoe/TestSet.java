package ss.week5.tictactoe;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestSet {
	
	private Set<Integer> set;
	private Iterator<Integer> it;

	public TestSet() {		
		this.set = new HashSet<Integer>();
		this.set.add(3);
		this.it = set.iterator();		
	}
	
	public int getNum() {
		return it.next();
	}
	
	public static void main(String[] args) {
		TestSet testSet = new TestSet();
		System.out.println("the first value is " + testSet.getNum());
	}
}
