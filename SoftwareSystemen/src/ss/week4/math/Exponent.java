package ss.week4.math;

public class Exponent implements Function, Integrandable {
	
	private int n;

	public Exponent(int n) {
		this.n = n;
	}

	@Override
	public double apply(double x) {
		return Math.pow(x, this.n);	
	}

	@Override
	public Function derivative() {
		Constant n = new Constant(this.n);
		Exponent x = new Exponent(this.n - 1);
		return new LinearProduct(n, x);
	}
	
	@Override
	public Function integrand() {
		Constant constant = new Constant(1 / (double)(this.n + 1));
		Exponent exponent = new Exponent(this.n + 1);
		return new LinearProduct(constant, exponent);
	}
	
	public String toString() {
		return "x^" + this.n;
	}

}
