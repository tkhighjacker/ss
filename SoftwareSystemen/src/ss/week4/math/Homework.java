package ss.week4.math;

public class Homework {

	public Homework() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		LinearProduct f1 = new LinearProduct(new Constant(4), new Exponent(4));
		Function f2 = f1.derivative();
		Function f3 = f1.integrand();
		System.out.println("f(x) = " + f1.toString() + ", f(8) =  " + f1.apply(8));
		System.out.println("f(x) = " + f2.toString() + ", f(8) =  " + f2.apply(8));
		System.out.println("f(x) = " + f3.toString() + ", f(8) =  " + f3.apply(8));

	}

}
