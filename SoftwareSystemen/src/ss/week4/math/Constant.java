package ss.week4.math;

public class Constant implements Function, Integrandable {
	
	private double value;

	public Constant(double x) {
		this.value = x;
	}
	
	@Override
	public double apply(double x) {
		return this.value;
	}
	
	@Override
	public Function derivative() {
		return new Constant(0);
	}
	
	@Override
	public Function integrand() {
		Constant constant = new Constant(this.value);
		Exponent exponent = new Exponent(1);
		return new LinearProduct(constant, exponent);
	}
	
	@Override
	public String toString() {
		return "" + this.value;
	}

}
