package ss.week4.math;

public class Product implements Function {
	
	protected Function fx;
	protected Function gx;

	public Product(Function fx, Function gx) {
		this.fx = fx;
		this.gx = gx;
	}

	@Override
	public double apply(double x) {
		return fx.apply(x) * gx.apply(x);
	}

	@Override
	public Function derivative() {
		Function fxd = fx.derivative();
		Function gxd = gx.derivative();
		return new Sum(new Product(fxd, gx), new Product(fx, gxd));
	}
	
	@Override
	public String toString() {
		return fx.toString() + " * " + gx.toString();
	}

}
