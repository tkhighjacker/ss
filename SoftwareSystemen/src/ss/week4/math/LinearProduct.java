package ss.week4.math;

public class LinearProduct extends Product implements Integrandable {
	
	public LinearProduct(Constant fx, Function gx) {
		super(fx, gx);
	}
	
	public Function derivative() {
		return new LinearProduct((Constant) fx, gx.derivative());
	}
	
	@Override
	public Function integrand() {
		Function result = null;
		if (this.gx instanceof Integrandable) {
			result = new LinearProduct((Constant) fx, ((Integrandable) gx).integrand());
		}
		return result;
	}
	
	@Override
	public String toString() {
		return fx.toString() + " * " + gx.toString();
	}

}
