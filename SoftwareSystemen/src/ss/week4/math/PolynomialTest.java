package ss.week4.math;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PolynomialTest {
	
	private static final double DELTA = 1e-10;
	private static final double[] CONSTANT_VALUE = new double[] {3, 5, 7, -3};
	private Polynomial polynomial;

	@Before
	public void setUp() throws Exception {
		this.polynomial = new Polynomial(CONSTANT_VALUE);
		
	}

	@Test
	public void testApply() {
		assertEquals(3, this.polynomial.apply(0), DELTA);
		assertEquals(0, this.polynomial.apply(3), DELTA);
	}
	
	@Test
	public void testDerivative() {
		assertEquals(-3, this.polynomial.derivative().apply(2), DELTA);
		assertEquals(10, this.polynomial.derivative().apply(1), DELTA);
		assertEquals(5, this.polynomial.derivative().apply(0), DELTA);
	}
	
	@Test
	public void testIntegrand() {
		assertEquals((68/3), this.polynomial.integrand().apply(2), DELTA);
		assertEquals(85/12, this.polynomial.integrand().apply(1), DELTA);
		assertEquals(0.0, this.polynomial.integrand().apply(0), DELTA);
	}
}
