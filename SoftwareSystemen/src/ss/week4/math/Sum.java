package ss.week4.math;

public class Sum implements Function, Integrandable {
	
	private Function fx;
	private Function gx;

	public Sum(Function fx, Function gx) {
		this.fx = fx;
		this.gx = gx;
	}

	@Override
	public double apply(double x) {
		return fx.apply(x) + gx.apply(x);
	}

	@Override
	public Function derivative() {
		return new Sum(fx.derivative(), gx.derivative());
	}
	
	@Override
	public Function integrand() {
		Function result = null;
		if (this.fx instanceof Integrandable && this.gx instanceof Integrandable) {
			result = new Sum(((Integrandable)fx).integrand(), ((Integrandable)gx).integrand());
		}
		return result;
	}
	
	@Override
	public String toString() {
		return fx.toString() + " + " + gx.toString();
	}

}
