package ss.week4.math;

public class Polynomial implements Function {
	
	private LinearProduct[] linearProductArray;
	
	public Polynomial(double[] linearProducts) {
		LinearProduct[] result = new LinearProduct[linearProducts.length];
		for (int i = 0; i < linearProducts.length; i++) {
			Exponent exponent = new Exponent(i);
			Constant constant = new Constant(linearProducts[i]);
			LinearProduct linearProduct = new LinearProduct(constant, exponent);
			result[i] = linearProduct;
		}
		this.linearProductArray = result;
	}

	@Override
	public double apply(double x) {
		double result = 0;
		for (int i = 0; i < linearProductArray.length; i++) {
			result = result + linearProductArray[i].apply(x);
		}
		return result;
	}

	@Override
	public Function derivative() {
		Function result = linearProductArray[0].derivative();
		for (int i = 1; i < linearProductArray.length; i++) {
			result = new Sum(result, linearProductArray[i].derivative());
		}
		return result;
	}
	
	public Function integrand() {
		Function result = linearProductArray[0].integrand();
		for (int i = 1; i < linearProductArray.length; i++) {
			result = new Sum(result, linearProductArray[i].integrand());
		}
		return result;
	}

}
