package ss.week4;

import java.util.*;

public class MergeSort {
    public static <Elem extends Comparable<Elem>> void mergesort(List<Elem> list) {
    	List<Elem> copyList = new ArrayList<>();
    	copyList.addAll(list);
    	List<Elem> left = new ArrayList<>();
    	List<Elem> right = new ArrayList<>();
    	
    	//split
    	if (copyList.size() > 1) {
    		int mid = copyList.size() / 2;
    		for(int i = 0; i < mid; i++) {
    			left.add(copyList.get(i));
    		}
    		for(int i = mid; i < copyList.size(); i++) {
    			right.add(copyList.get(i));
    		}
    		System.out.println("left list after split" + left.toString());
    		System.out.println("right list after split" + right.toString());
    		System.out.println("--------------------------------");
    		
    		System.out.println("Mergesort right");
    		mergesort(right);
    		System.out.println("Mergesort left");
    		mergesort(left);

    		
    		int leftIndex = 0;
    		int rightIndex = 0;
    		int listIndex = 0;
    		while (leftIndex < left.size() && rightIndex < right.size()) {
    			if (left.get(leftIndex).compareTo(right.get(rightIndex)) < 0) {
    				System.out.println("leftindex: " + leftIndex);
    				System.out.println(left.toString());
    				System.out.println("listindex: " + listIndex);
    				System.out.println("add " + left.get(leftIndex).toString() + " to " + copyList.toString());
    				copyList.set(listIndex, left.get(leftIndex)); 
    				System.out.println("new list:" + copyList.toString());
    				System.out.println("--------------------------------");
    				leftIndex++;
    			} else {
    				System.out.println("rightindex " + rightIndex);
    				System.out.println(right.toString());
    				System.out.println("listindex " + listIndex);
    				System.out.println("add " + right.get(leftIndex).toString() + " to " + copyList.toString());
    				copyList.set(listIndex, right.get(rightIndex));
    				System.out.println("new list:" + copyList.toString());
    				System.out.println("--------------------------------");
    				rightIndex++;
    				
    			}
    			listIndex++;
    		}
    	}
    	list = copyList;
    	
    }
    
    public <Elem extends Comparable<Elem>> List<Elem> merge(List<Elem> left, List<Elem> right) {
    	
    }
    
    public static void main(String[] args) {
    	List<Integer> testList = new ArrayList<Integer>();
    	testList.add(5);
    	testList.add(4);
    	testList.add(1);
    	testList.add(2);
    	testList.add(3);
    	System.out.println("Current list: " + testList.toString());
    	System.out.println("--------------------------------");
    	MergeSort.mergesort(testList);
    	testList.toString();
    	
    }
}
